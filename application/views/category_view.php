<!DOCTYPE html>
<html lang="en">
    <head>
        
        <title>Category view</title>

    <style type="text/css">
        <?php
        require_once(APPPATH . 'assets/css/bootstrap.css');
        require_once(APPPATH . 'assets/css/bootstrap.min.css');
        require_once(APPPATH . 'assets/css/bootstrap-theme.min.css');
        require_once(APPPATH . 'assets/css/customTh.css');
        ?>          
    </style>

    <script>
        <?php
        require_once(APPPATH . 'assets/js/jquery.min.js');
        require_once(APPPATH . 'assets/js/bootstrap.min.js');
        ?>
            
            
            
         $(document).ready(function() {
        
        //hideall(); 
    
    
    var navListItems = $('ul.setup-panel li a'),
        allWells = $('.setup-content');

    allWells.hide();

    navListItems.click(function(e)
    {
       e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this).closest('li');
        
        if (!$item.hasClass('disabled')) 
        {
            navListItems.closest('li').removeClass('active');
            $item.addClass('active');
            allWells.hide();
            $target.show(); 
        }
  
    });
    
    $('ul.setup-panel li.active a').trigger('click');
    
    // next ONLY //
    $('#activate-step-2').on('click', function(e) {
        $('ul.setup-panel li:eq(1)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-2"]').trigger('click');
       // $(this).remove();
    })
    
    
    $('#activate-step-3').on('click', function(e) {
        $('ul.setup-panel li:eq(2)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-3"]').trigger('click');
        //$(this).remove();
    })
    
    $('#activate-step-4').on('click', function(e) {
        $('ul.setup-panel li:eq(2)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-3"]').trigger('click');
        //$(this).remove();
    })
    
    
    // back //
    
        $('#back-step-1').on('click', function(e) {
        $('ul.setup-panel li:eq(2)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-1"]').trigger('click');
       // $(this).remove();
        })
    
        $('#back-step-2').on('click', function(e) {
        $('ul.setup-panel li:eq(3)').removeClass('disabled');
        $('ul.setup-panel li a[href="#step-2"]').trigger('click');
       // $(this).remove();
        })
    
});
            
            
            
            
            
            
            
            
    </script>
    </head>
    <body>
         
             <div class="form-group">
                <label class="col-sm-2 control-label">Title</label> 
             <div class="input-group"><input type="text" id="title"   class="form-control input-sm" data-validation-required-message="Please enter a Value ." required> </div>
            </div>
        
        <?php            
            if (isset($catFields)) {                          
              foreach ($catFields as $row)
              {
                  
                $attributeid = $row->attributeid;
                $attributename = $row->attributename;
                $htmlcomponent = $row->htmlcomponent;

                
                if($htmlcomponent == "Text Field")
                {
                 
        ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $attributename ?></label> 
                                <div class="input-group"><input type="text" id="<?php echo $attributeid ?>"   class="form-control input-sm" data-validation-required-message="Please enter a Value ." required> </div>
                            </div>
        
        
        <?php
                }
                else if($htmlcomponent=="Drop Down")
                {
                    ?>
                        
                    <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $attributename ?></label>
                                    <select class="input-group" id="<?php echo $attributeid ?>" name="<?php echo $fieldName ?>">
                                     <?php            
                                    if (isset($catselect)) {                          
                                    foreach ($catselect as $row2)
                                    { 
                                        
                                        $attributeid2 = $row2->attributeid;
                                        $dropdownvalues2 = $row2->dropdownvalues;
                                        $valueid2 = $row2->valueid;
                                        
                                        if($attributeid==$attributeid2)
                                        {
                                            
                                      ?>   
                                    <option value="<?php echo $valueid2 ?>"><?php echo $dropdownvalues2 ?></option>
                                    
                                    <?php
                                        }
                                    }
                                   }
                                    ?>
                                    </select>
                     </div>      
                    <?php
                }

              }
            }
            ?>
         <div class="form-group">
                                    <label class="col-sm-2 control-label">Description</label> 
                                    <div class="input-group"><textarea class="form-control" rows="4" id="description" name="description" style="width:400px" ></textarea> </div>
         </div>
        
         <div class="form-group">
             <label class="col-sm-2 control-label">Price</label> 
             <div class="input-group"><input type="text" id="price"   class="form-control input-sm" data-validation-required-message="Please enter a Value ." required> </div>
        </div>
        
    </body>
    
    
</html>