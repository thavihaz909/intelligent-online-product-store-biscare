<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Get started with Kstore</title>

        <?php require_once (APPPATH . 'views/common/header_th.php'); ?>

    </head>

    <body style="background-color:#eee;">
        <div class="container" style="width:500px; margin:0 auto; padding: 15px;">
            <!-- 			<div class="alert alert-success"></div> -->
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Welcome to Kstore, <?php echo $first_name ?>!</h3>
                </div>
                <div class="panel-body">
                    Your registration is completed successfully!
                    You can login to Kstore using <b><?php echo $email ?>
                        <br>
                        <form role="form" action="<?php echo base_url(); ?>" method="post">
                            <input style="margin-right: 10px; margin-top: 15px;" type="submit" class="btn btn-primary" value="Continue">
                        </form>
                </div>
            </div>		
        </div>
         <?php require_once (APPPATH . 'views/common/footer_th.php'); ?>
    </body>
</html>	