<ul class="nav nav-pills nav-stacked">
    <li <?php if(isset($account)&&!empty($account)&&$account=='public'){ echo 'class="active"';}?>><a href="<?php echo base_url("/my_account"); ?>">Public Profile</a></li>
    <li <?php if(isset($account)&&!empty($account)&&$account=='edit'){ echo 'class="active"';}?>><a href="<?php echo base_url("/profileupdate"); ?>">Account Information</a></li>
    <li><a href="#">Purchasing History</a></li>
    <li <?php if(isset($account)&&!empty($account)&&$account=='subscription'){ echo 'class="active"';}?>><a href="<?php echo base_url("/my_subscriptions"); ?>">Subscriptions</a></li>
</ul>
