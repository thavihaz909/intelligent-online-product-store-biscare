<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>New AD</title>
        <style>
            
            
.easyWizardSteps {list-style:none;width:100%;overflow:hidden;margin:0;padding:0;border-bottom:1px solid #ccc;margin-bottom:20px}
.easyWizardSteps li {font-size:18px;display:inline-block;padding:10px;color:#B0B1B3;margin-right:20px;}
.easyWizardSteps li span {font-size:24px}
.easyWizardSteps li.current {color:#000}
.easyWizardButtons {overflow:hidden;padding:10px;}
.easyWizardButtons button, .easyWizardButtons .submit {cursor:pointer}
.easyWizardButtons .prev {float:left}
.easyWizardButtons .next, .easyWizardButtons .submit {float:right}
            
            
<?php
require_once (APPPATH . 'assets/css/bootstrap-theme.css');
require_once (APPPATH . 'assets/css/bootstrap-theme.min.css');
require_once (APPPATH . 'assets/css/bootstrap.css');
require_once (APPPATH . 'assets/css/bootstrap.min.css');
require_once (APPPATH . 'assets/css/custom.css');
?>
        </style>
        <script>
<?php
require_once(APPPATH . 'assets/js/jqBootstrapValidation.js');
require_once(APPPATH . 'assets/js/jquery.min.js');
require_once(APPPATH . 'assets/js/bootstrap.min.js');
require_once(APPPATH . 'assets/js/jquery.easyWizard.js');
?>
    
    $('#myWizard').easyWizard();
    
    
        </script>
    </head>
    <body>
        
        <form id="myWizard" type="get" action="" class="form-horizontal">
    <section class="step" data-step-title="The first">
        <div class="control-group">
            <label class="control-label" for="inputEmail">Email</label>
            <div class="controls">
                <input type="text" id="inputEmail" placeholder="Email" class="input-xlarge" title="Email is required !" required >
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputPassword">Password</label>
            <div class="controls">
                <input type="password" id="inputPassword" placeholder="Password" class="input-xlarge">
            </div>
        </div>
    </section>
    <section class="step" data-step-title="The second">
        <div class="control-group">
            <label class="control-label" for="inputUsername">Username</label>
                <div class="controls">
                <input type="text" id="inputUsername" placeholder="Username" class="input-xlarge">
            </div>
        </div>
    </section>
    <section class="step" data-step-title="The third">
        <div class="control-group">
            <label class="control-label" for="inputFirstname">Firstname</label>
                <div class="controls">
                <input type="text" id="inputFirstname" placeholder="Firstname" class="input-xlarge">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputCity">City</label>
                <div class="controls">
                <input type="text" id="inputCity" placeholder="City" class="input-xlarge">
            </div>
        </div>
    </section>
</form>
        
    </body>
</html>
