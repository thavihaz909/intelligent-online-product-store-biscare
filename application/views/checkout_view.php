<!DOCTYPE html>
<html lang="en">
    <head>
        <?php require_once (APPPATH . 'views/common/header_th.php'); ?>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>
            $(document).ready(function(){
                $("#createaccount").click(function(){
                    $("#passwordview").slideToggle("slow");
                });
            });
        </script>
        <title>Payment Details</title>
    </head>
    <body class="body-custom">
        <?php require_once (APPPATH . 'views/common/nav_bar.php'); ?>

        <div class="container cont-cust">
            <?php
            if ($this->session->userdata('logged_in')) {
                require_once (APPPATH . 'views/checkout_address_view.php');
            } else {
                require_once (APPPATH . 'views/checkout_billing_details_view.php');
            }
            ?>              
        </div>        
    </body>
</html>