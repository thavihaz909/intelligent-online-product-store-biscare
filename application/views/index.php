<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?php echo base_url() ?>images/favicon.ico" type="image/x-icon"/>

        <title>KStore</title>
        <?php $this->load->helper('url'); ?>

        <style type="text/css">
<?php
require_once('assets/css/signup.css');
require_once('assets/css/custom-th.css');
?>
        </style>
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">


        <script>
            $(function () { $("input").not("[type=submit]").jqBootstrapValidation(); } );
        </script>

        <script>
<?php
require_once(APPPATH . 'assets/js/jquery.min.js');
?>
        </script>

    </head>

    <body>
        <?php $GLOBALS['main_nav'] = 'index' ?>
        <?php require_once (APPPATH . 'views/common/nav_bar.php'); ?> <!-- Navigation Menu -->

        <div class="container" style="margin: 20px auto;">

            <!-- Search Box -->
            <div class="cont-search-cust row">
                <form class="form-horizontal" role="form" action="<?php echo base_url(); ?>browse_gallery" >
                    <div class="form-group" style="margin:0 auto;">
                        <div class="col-md-8">
                            <input type="text" name="keyword" class="form-control" id="search" placeholder="Search...">
                        </div>
                        <div class="col-md-3">
                            <select name="catid" class="form-control">
                                <option value="0">All Categories</option>
                                <?php
                                if (isset($top) && !empty($top)) {
                                    foreach ($top as $key => $row) {?>
                                        <option value=<?php echo $row['categoryid']; ?>><?php echo $row['categoryname']; ?></option>
                           <?php    }
                                }
                            ?>

                            </select>
                        </div>
                        <div class="col-md-1">
                            <button class="btn btn-primary">Search</button>
                        </div>    
                    </div>
                </form>   
            </div>

            <!-- Category Menu -->

            <ul id="cat-menu" class="nav nav-pills nav-justified category-bar">
                <?php
                if (isset($cats)) {
                    $cat_temp = array();
                    $prio_temp = array();
                    foreach ($cats as $key => $row) {
                        $cat_temp[$key] = $row['parentcategory'];
                        $prio_temp[$key] = $row['priority'];
                    }
                    array_multisort($cat_temp, SORT_ASC, $prio_temp, SORT_ASC, $cats);

                    $cats_inner = $cats;
                }
                foreach ($cats as $cat) {
                    if ($cat['parentcategory'] == 0) {
                        $cat_id = $cat['categoryid'];
                        ?>
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown"><?php echo $cat['categoryname'] ?></a>
                            <ul class="dropdown-menu">
                                <?php
                                foreach ($cats_inner as $cat_inner) {
                                    if ($cat_inner['parentcategory'] == $cat_id) {
                                        $inner_cat_id = $cat_inner['categoryid'];
                                        ?>          
                                        <li><a href='<?php echo base_url("/browse_gallery?catid=") . urldecode($inner_cat_id); ?>'><?php echo $cat_inner['categoryname'] ?></a></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>


            <!-- Products Slider -->

            <div class="row cont-slider-cust">
                <div class="row">
                    <div class="col-md-9">
                        <h3>
                            You may be interested in</h3>
                    </div>
                    <div class="col-md-3">
                        <!-- Controls -->
                        <div class="controls pull-right hidden-xs">
                            <a class="left fa fa-chevron-left btn btn-success" href="#product-slider"
                               data-slide="prev"></a><a class="right fa fa-chevron-right btn btn-success" href="#product-slider"
                               data-slide="next"></a>
                        </div>
                    </div>
                </div>
                <div id="product-slider" class="carousel slide hidden-xs" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">

                        <?php
                        if (isset($ads)) {
                            
                        }
                        $ad_count = 0;
                        foreach ($ads as $ad) {
                            if ($ad_count == 0) {
                                echo '<div class="item active">';
                                echo '<div class="row">';
                            } else if ($ad_count % 4 == 0) {
                                echo '<div class="item">';
                                echo '<div class="row">';
                            }
                            $ad_count++;
                            $img_url = base_url("/images/uploads") . '/' . $ad['image'];
                            ?>
                            <div class="col-sm-3">
                                <div class="col-item">
                                    <div class="photo">
                                        <img src='<?php echo $img_url; ?>' class="img-responsive lazy" alt='<?php echo $ad['title']; ?>' />
                                    </div>
                                    <div class="info" style="position: absolute; bottom: 0;">
                                        <div class="row">
                                            <div class="price">
                                                <h5>
                                                    <?php echo $ad['title']; ?></h5>
                                                <h5 class="price-text-color">
    <?php echo 'Rs. ' . $this->cart->format_number($ad['price']); ?></h5>
                                            </div>
                                        </div>
                                        <div class="separator clear-left">
                                            <p class="btn-add">
                                                <i class="fa fa-shopping-cart"></i><a href="http://www.jquery2dotnet.com" class="hidden-sm">Add to cart</a></p>
                                            <p class="btn-details">
                                                <i class="fa fa-list"></i><a  class="hidden-sm" href=<?php echo base_url("/ad_details?adid=") . urldecode($ad['adid']); ?>>More details</a></p>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            if ($ad_count % 4 == 0) {
                                echo '</div>';
                                echo '</div>';
                            }
                        }
                        ?>

                    </div>
                </div>

            </div>
            <hr>

            <footer>
                <p><small><strong>&copy; Kstore 2014</strong></small></p>

            </footer>

        </div>
    </div>

</div> <!-- /container -->        


<script>
        !function ($) {
        $(function(){
            // carousel demo
            $('#product-slider').carousel()
        })
    }(window.jQuery)
</script>

<?php require_once (APPPATH . 'views/common/footer_th.php'); ?>


</body>
</html>
