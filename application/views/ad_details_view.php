<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="See Moreport" content="width=device-width, initial-scale=1.0">
        <?php
        if (isset($ad) && isset($seller) && isset($images)) {
            
            foreach ($ad as $ad_info) {
                $date=explode(" ", $ad_info['datesubmitted']);
            }
            foreach ($seller as $seller_info) {
                
            }
            ?>
            <title><?php echo $ad_info['title'] ?></title>
            <?php require_once (APPPATH . 'views/common/header_th.php'); ?>

            <style type="text/css">
    <?php
    require_once(APPPATH . 'assets/css/smoothproducts.css');
    ?>          
            </style>


        </head>

        <body class="body-custom">
            <?php $GLOBALS['main_nav'] = 'ad details' ?>
            <?php require_once (APPPATH . 'views/common/nav_bar.php'); ?>
                <div class="container cont-cust">
                <div class="row">
                    <div class="col-md-4">
                        <div class="sp-wrap image-box-custom">
                            <?php
                            foreach ($images as $image) {
                                $img_url = base_url("/images/uploads") . '/' . $image['imageurl'];
                                ?>
                                <a href=<?php echo $img_url ?>><img src='<?php echo $img_url ?>' class="lazy" alt='<?php $img_url ?>'></a>
                            <?php } ?>
                        </div>
                    </div>
                    <?php echo form_open("cart/add_cart_item"); ?>
                    <div class="col-md-7">
                        <div class="row">
                            <h3><?php echo $ad_info['title'] ?></h3>
                            <input type="hidden" name="ad_id" value=<?php echo $ad_info['adid']; ?>/>
                            <hr>
                        </div>
                        <div class="row sub-container">
                            <dl class="dl-horizontal">
                                <dt>Price:</dt>
                                <dd><?php echo $this->cart->format_number($ad_info['price']) ?></dd>
                                <dt>Available:</dt>
                                <dd><?php echo $ad_info['quantity'].' Pcs.' ?></dd>
                                <dt>Location:</dt>
                                <dd><?php echo $ad_info['location'] ?></dd>
                                <dt>Posted On:</dt>
                                <dd><?php echo $date[0] ?></dd>
                            </dl>
                        </div>
                        <div class="row">
                            <div class="col-xs-2">
                                <label for="qty" class="control-label" >Quantity</label>
                                <input type="text" name="qty" id="qty" class="form-control" maxlength="4" size="4" value="1">
                            </div>
                            <div class="col-xs-10">
                                <div class="row">
                                    <label>  </label>
                                </div>
                                <div class="row">
                                    <input type="submit" name="submmit" class="btn btn-primary" value="Add to Cart"/>
                                    <!--button type="button" class="btn btn-success">Buy Now</button-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>

                </div>
                <div class="row" style="margin-top:30px;">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#description" data-toggle="tab">Item Details</a></li>
                        <li><a href="#seller" data-toggle="tab">Seller Details</a></li>
                        <li><a href="#reviews" data-toggle="tab">Reviews</a></li>
                    </ul>
                    <div class="tab-content">

                        <div class="tab-pane  tab-content-custom active" id="description">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Item Specification</h3>
                                </div>
                                <div class="panel-body">
                                    <dl class="dl-horizontal">
                                        <dt>Condition:</dt>
                                        <dd>Brand New</dd>
                                        <dt>Brand:</dt>
                                        <dd>Samsung</dd>
                                        <dt>Color:</dt>
                                        <dd>Black</dd>
                                    </dl>
                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Description</h3>
                                </div>
                                <div class="panel-body">
                                    <pre scrolling="no" class="description-cont"><?php echo $ad_info['description'] ?></pre>
                                </div>
                            </div> 
                        </div>


                        <div class="tab-pane tab-content-custom" id="seller">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Seller Details</h3>
                                </div>
                                <div class="panel-body">
                                    <dl class="dl-horizontal">
                                        <dt>Name:</dt>
                                        <dd><a href = <?php echo base_url("/my_account?userid=") . urldecode($seller_info['userid']); ?>><?php echo $seller_info['first_name'].' '.$seller_info['last_name'] ?></a></dd>
                                        <dt>Email:</dt>
                                        <dd><?php echo $seller_info['email'] ?></dd>
                                        <dt>Contact No:</dt>
                                        <dd><?php echo $seller_info['telephone'] ?></dd>
                                        <dt>Location:</dt>
                                        <dd><?php echo $seller_info['city'] ?></dd>
                                    </dl>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane tab-content-custom" id="reviews">
                            <div class="alert alert-warning">
                                No reviews are available for this item!
                            </div>
                        </div>
                    </div>
                </div>

                <?php require_once (APPPATH . 'views/common/footer_th.php'); ?>
                <script type="text/javascript">
                 
                    <?php require_once (APPPATH . 'assets/js/smoothproducts.js'); ?>
                    /* wait for images to load */
                    $(window).load( function() {
                        $('.sp-wrap').smoothproducts();
                    });
                </script>
            </div>
                
            <?php
        } else {
            echo "You tried to access this page in wrong way!";
        }
        ?>
    </body>
</html> 