<?php

class Browse_Gallery_Model extends CI_Model {

    private $all_ads = array();
    private $cat_path = array();

    function get_gallery_ads() {
        $this->db->select()->from('ads')->where('status', 'active')->order_by("datesubmitted", "desc")->limit(12);
        $query = $this->db->get();
//        $result = $query->result_array();
//        $rows = $this->sort_by_date($result);
//        return $rows;
        return $query->result_array();
    }

    function get_gallery_ads_bycat($catid) {
        $this->get_gallery_ads_recurse($catid);
        $rows = $this->sort_by_date($this->all_ads);
        return $rows;
    }

    function get_gallery_ads_recurse($catid) {
        $this->db->select()->from('category')->where('parentcategory', $catid);
        $query = $this->db->get();
        $is_subcat = false;

        foreach ($query->result_array() as $row) {
            $is_subcat = true;
            $subcat_id = $row['categoryid'];
            $this->get_gallery_ads_bycat($subcat_id);
        }

        if ($is_subcat == false) {
            $this->db->select()->from('ads')->where(array('categoryid' => $catid, 'status' => 'active'));
            $query = $this->db->get();
            $result = $query->result_array();
            $all_ads = $this->all_ads;
            $this->all_ads = array_merge_recursive($all_ads, $result);
        }
    }

    function get_category_name($catid) {
        $this->db->select()->from('category')->where('categoryid', $catid);
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_all_categories() {
        $this->db->select()->from('category');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get_category_path($catid) {
        $this->get_category_path_recurse($catid);
        return $this->cat_path;
    }

    function get_category_path_recurse($catid) {
        $this->db->select()->from('category')->where('categoryid', $catid);
        $query = $this->db->get();

        foreach ($query->result_array() as $row) {
            $parent_id = $row['parentcategory'];
            $result = $query->result_array();
            $cat_path = $this->cat_path;
            $this->cat_path = array_merge_recursive($cat_path, $result);

            if ($parent_id != 0) {
                $this->get_category_path_recurse($parent_id);
            }
        }
    }

    function get_child_categories($catid) {
        $this->db->select()->from('category')->where('parentcategory', $catid);
        $query = $this->db->get();
        return $query->result_array();
    }

    function sort_by_date($rows) {
        $date = array();
        foreach ($rows as $key => $row) {
            $date[$key] = $row['datesubmitted'];
        }
        array_multisort($date, SORT_DESC, $rows);

        return $rows;
    }

    function insert_user_ip($data) {
        $this->db->insert('ip_records', $data);
        return $this->db->insert_id();
    }
    
    function get_top_categories() {
        $this->db->select()->from('category')->where('istopcategory', 1)->order_by("categoryname", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

}

