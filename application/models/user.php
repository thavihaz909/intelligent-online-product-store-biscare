
<?php

Class User extends CI_Model {

    function login($email, $password) {
        $this->db->select('userid, first_name, last_name, email, password');
        $this->db->from('users');
        $this->db->where('email', $email);
        $this->db->where('password', MD5($password));
        $this->db->limit(1);

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    function insert_user($data) {
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }

    function verify_user_email($email) {
        $this->db->select('email')->from('users')->where('email', $email);
        $query = $this->db->get();
        return $query->row_array();
    }

}

