<?php

class Browse_Gallery extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('browse_gallery_model');
    }

    public function index($start = 0) {
        if (isset($_GET['catid'])) {
            $data['cat_path'] = $this->browse_gallery_model->get_category_path($_GET['catid']);
            $data['child_cats'] = $this->browse_gallery_model->get_child_categories($_GET['catid']);
            $data['ads'] = $this->browse_gallery_model->get_gallery_ads_bycat($_GET['catid']);
            $data['cat'] = $this->browse_gallery_model->get_category_name($_GET['catid']);
            $data['top'] = $this->browse_gallery_model->get_top_categories();
            if($_GET['catid'] != 0){
                $this->submit_ip_details();
            }
            $this->load->view('browse_gallery_view', $data);
        } else {
            $_GET['catid'] = 0;
            $this->index();
        }
    }

    public function submit_ip_details() {
        $data['ip'] = $this->input->ip_address();
        $data['value'] = $_GET['catid'];
        $data['type'] = 'catid';
        if ($this->session->userdata('logged_in')) {
            $data['userid']=$this->session->userdata['logged_in']['userid'];
        } 
        $this->browse_gallery_model->insert_user_ip($data);
    }

}

?>
