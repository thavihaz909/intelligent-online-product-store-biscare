<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Cart
 *
 * @author Hasith Malinga
 */
class Cart extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->library('cart');
        $this->load->model('cart_model');
    }

    function index() {
        $this->load->view('test_cart');
    }

    function add_cart_item() {
        //$post= $this->input->post();
        
        //var_dump($post);
        if ($this->cart_model->validate_add_cart_item() == TRUE) {

            
            //if ($this->input->post('ajax') != '1') {
                redirect('cart');
            //} else {
               // echo 'true'; 
            //}
        }
        else{
            //alert("No data");
            //redirect('cart','refresh');
        }
    }
    
    function update_cart(){
        if($this->input->post('continue')){
            redirect(base_url('/browse_gallery'));
        }
        
        if($this->input->post('update')){
            $this->cart_model->validate_update_cart();
            redirect('cart');
        }
        
        if($this->input->post('checkout')){
            //redirect(base_url('/checkout'));
            //$this->load->view('checkout_view');
        }
    }

    

}

/* End of file Cart.php */
/* Location: ./application/controllers/Cart.php */
