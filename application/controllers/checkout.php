<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Checkout
 *
 * @author Hasith Malinga
 */
class Checkout extends CI_Controller {
    
    function __construct() 
    {
        parent::__construct();        
    }
    
    function index(){
        $this->load->view('checkout_view');
    }
}

/* End of file Checkout.php */
/* Location: ./application/controllers/Checkout.php */
