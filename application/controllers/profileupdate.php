<?php

class Profileupdate extends CI_Controller {

//    function Profileupdate() {
//        parent::Controller();
//    }
//    public $fname=array();
//    public $lname=array();

    function index() {
	    $session_data = $this->session->userdata('logged_in');
        $this->load->model('mprofileupdate');
        $data = $this->mprofileupdate->get_user_info($session_data);
        //$this->load->view('header');
        $this->load->view('profile_update', $data);
        //$this->load->view('footer');
    }

    function updateprofileinfogeneral() {
        if ($_POST) {
            $data = array(
                'table_name' => 'users',
                'fname' => $_POST['inputFirstName'],
                'lname' => $_POST['inputLastName'],
                'email' => $_POST['inputEmail'],
                'phone' => $_POST['inputPhone'],
                'question' => $_POST['inputSecurityQuestion'],
                'answer' => $_POST['inputSecurityAnswer']
            );

            $this->load->model('mprofileupdate'); // load the model first
            if ($this->mprofileupdate->upddatagenaral($data)) { // call the method from the model
                echo "Update Successfully";
            } else {
                echo "Update Unsuccessfully";
            }
        } else {
            echo 'forbidden access';
        }
    }

    function updateprofileinfoaddress() {
//        $var=$this->input->post('inputFirstName');
//        echo $var;
		$session_data = $this->session->userdata('logged_in');
        if ($_POST) {
            $data = array(
                'table_name' => 'users',
                'address1' => $_POST['inputLine01'],
                'address2' => $_POST['inputLine02'],
//                'email' => $_POST['inputEmail'],
                'email' => $session_data['email'],
                'province' => $_POST['inputProvince'],
                'city' => $_POST['inputCity']
            );

            $this->load->model('mprofileupdate'); // load the model first
            if ($this->mprofileupdate->upddataaddress($data)) { // call the method from the model
                echo "Update Successfully";
            } else {
                echo "Update Unsuccessfully";
            }
        } else {
            echo 'forbidden access';
        }
    }

}
