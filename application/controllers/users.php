<?php

class Users extends CI_Controller {

    private $data;
    private $errorlist;

    function __construct() {
        parent::__construct();
        $this->load->model('user');
    }

    public function index() {
        $this->load->view('user_registration');
    }

    function new_user() {
        if ($_POST) {
            $this->retrieve_values();
            $this->verify_captcha();
            $this->verify_email($this->data);

            if (!isset($this->errorlist)) {
                $this->user->insert_user($this->data);
                $this->load->view('user_registration_success', $this->data);
                // $this->send_confirmation();
            } else {
                $this->load->view('user_registration', $this->errorlist);
            }
        } else {
            echo 'Forbidden Access';
        }
    }

    function retrieve_values() {
        $this->data = array(
            'first_name' => $_POST['fname'],
            'last_name' => $_POST['lname'],
            'email' => $_POST['email'],
            'password' => md5($_POST['password']),
            'hash' => md5(rand(0, 1000))
        );
    }

    function verify_captcha() {
        require_once(APPPATH . 'assets/libs/recaptchalib.php');
        $privatekey = "6LfRQu8SAAAAAFqit_3icb5rVYX_C0bDv0sGSKhi";
        $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

        if (!$resp->is_valid) {
            $this->errorlist['captcha'] = 1;
        }
    }

    function verify_email() {
        $user_exist = $this->user->verify_user_email($this->data['email']);

        if ($user_exist) {
            $this->errorlist['email'] = 1;
        }
    }

    function send_confirmation() {
        /* must make an alternation to php.ini file : 
          change ;extension=php_openssl.dll to extension=php_openssl.dll */

        require_once(APPPATH . 'assets/libs/PHPMailer-master/class.phpmailer.php');

        $mail = new PHPMailer();
        //$baseurl			=base_url();

        $mail->IsSMTP(); // telling the class to use SMTP
        $mail->SMTPDebug = 2;                     // enables SMTP debug information (for testing)
        // 1 = errors and messages
        // 2 = messages only
        $mail->SMTPAuth = true;                  // enable SMTP authentication
        $mail->SMTPSecure = "tls";                 // sets the prefix to the servier
        $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
        $mail->Port = 587;                   // set the SMTP port for the GMAIL server
        $mail->Username = "kstore.ltd@gmail.com";  // GMAIL username
        $mail->Password = "kstorepass";            // GMAIL password

        $mail->SetFrom('kstore.ltd@gmail.com', 'First Last');

        $mail->AddReplyTo("kstore.ltd@gmail.com", "First Last");

        $mail->Subject = "Welcome to Kstore!";

        $mail->Body = '
 
			Thanks for signing up!
			Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below.
			 
			------------------------
			Email: ' . $this->data['email'] . '
			Password: ' . $_POST['password'] . '
			------------------------
			 
			Please click this link to activate your account:
			' . base_url() . 'index.php/users/verification_successful.php?email=' . $this->data['email'] . '&hash=' . $this->data['hash'] . '
			 
			'; // Our message above including the link
        //$mail->MsgHTML($body);

        $address = $this->data['email'];
        $mail->AddAddress($address, $this->data['first_name']);

        //$mail->AddAttachment("images/phpmailer.gif");      // attachment
        //$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

        if (!$mail->Send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo "Message sent!";
        }
    }

}

