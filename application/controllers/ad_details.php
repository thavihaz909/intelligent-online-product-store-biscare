<?php

class Ad_Details extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('ad_details_model');
    }
    
    public function index() {
        if(!isset($_GET['adid'])) {
            echo "Page not Found!";
        } else {
            $data['ad'] = $this->ad_details_model->get_ad_details($_GET['adid']);
            $data['images'] = $this->ad_details_model->get_ad_images($_GET['adid']);
            foreach ($data['ad'] as $value) {
                $seller=$value['sellerid'];
            }
            $data['seller'] = $this->ad_details_model->get_seller_details($seller);
            $this->submit_ip_details();
            $this->load->view('ad_details_view', $data);
        }
    }
    
    public function submit_ip_details() {
        $data['ip'] = $this->input->ip_address();
        $data['type'] = 'adid';
        $data['value'] = $_GET['adid'];
        if ($this->session->userdata('logged_in')) {
            $data['userid']=$this->session->userdata['logged_in']['userid'];
        } 
        $this->ad_details_model->insert_user_ip($data);
    }
}

