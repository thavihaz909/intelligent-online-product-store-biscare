<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('browse_gallery_model');
 }
  
  function index()
  {
     $data['ads'] = $this->browse_gallery_model->get_gallery_ads();
     $data['cats'] = $this->browse_gallery_model->get_all_categories();
     $data['top'] = $this->browse_gallery_model->get_top_categories();
     $this->load->view('index', $data);
  }
}
